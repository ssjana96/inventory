﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SearchCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TransactionTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SearchTransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LogOutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.SupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RegisterSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RegisterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SearchToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.UpdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PurchaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SellToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MakeTransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MakeTransactionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CustomerToolStripMenuItem
        '
        Me.CustomerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewCustomerToolStripMenuItem, Me.SearchCustomerToolStripMenuItem, Me.EditCustomerToolStripMenuItem})
        Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
        Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.CustomerToolStripMenuItem.Text = "Customer"
        '
        'NewCustomerToolStripMenuItem
        '
        Me.NewCustomerToolStripMenuItem.Name = "NewCustomerToolStripMenuItem"
        Me.NewCustomerToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.NewCustomerToolStripMenuItem.Text = "New Customer"
        '
        'SearchCustomerToolStripMenuItem
        '
        Me.SearchCustomerToolStripMenuItem.Name = "SearchCustomerToolStripMenuItem"
        Me.SearchCustomerToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.SearchCustomerToolStripMenuItem.Text = "Search Customer"
        '
        'EditCustomerToolStripMenuItem
        '
        Me.EditCustomerToolStripMenuItem.Name = "EditCustomerToolStripMenuItem"
        Me.EditCustomerToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.EditCustomerToolStripMenuItem.Text = "Edit Customer"
        '
        'TransactionToolStripMenuItem
        '
        Me.TransactionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TransactionTypeToolStripMenuItem, Me.SearchTransactionToolStripMenuItem})
        Me.TransactionToolStripMenuItem.Name = "TransactionToolStripMenuItem"
        Me.TransactionToolStripMenuItem.Size = New System.Drawing.Size(81, 20)
        Me.TransactionToolStripMenuItem.Text = "Transaction"
        '
        'TransactionTypeToolStripMenuItem
        '
        Me.TransactionTypeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PurchaseToolStripMenuItem, Me.SellToolStripMenuItem})
        Me.TransactionTypeToolStripMenuItem.Name = "TransactionTypeToolStripMenuItem"
        Me.TransactionTypeToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.TransactionTypeToolStripMenuItem.Text = "Transaction Type"
        '
        'SearchTransactionToolStripMenuItem
        '
        Me.SearchTransactionToolStripMenuItem.Name = "SearchTransactionToolStripMenuItem"
        Me.SearchTransactionToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.SearchTransactionToolStripMenuItem.Text = "Search Transaction"
        '
        'LogOutToolStripMenuItem
        '
        Me.LogOutToolStripMenuItem.Name = "LogOutToolStripMenuItem"
        Me.LogOutToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.LogOutToolStripMenuItem.Text = "LogOut"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SupplierToolStripMenuItem, Me.CustomerToolStripMenuItem, Me.TransactionToolStripMenuItem, Me.LogOutToolStripMenuItem, Me.ItemsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(909, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SupplierToolStripMenuItem
        '
        Me.SupplierToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegisterSupplierToolStripMenuItem, Me.EditSupplierToolStripMenuItem, Me.SearchToolStripMenuItem})
        Me.SupplierToolStripMenuItem.Name = "SupplierToolStripMenuItem"
        Me.SupplierToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.SupplierToolStripMenuItem.Text = "Supplier"
        '
        'RegisterSupplierToolStripMenuItem
        '
        Me.RegisterSupplierToolStripMenuItem.Name = "RegisterSupplierToolStripMenuItem"
        Me.RegisterSupplierToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.RegisterSupplierToolStripMenuItem.Text = "Register Supplier"
        '
        'EditSupplierToolStripMenuItem
        '
        Me.EditSupplierToolStripMenuItem.Name = "EditSupplierToolStripMenuItem"
        Me.EditSupplierToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.EditSupplierToolStripMenuItem.Text = "Edit Supplier"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.SearchToolStripMenuItem.Text = "Search"
        '
        'ItemsToolStripMenuItem
        '
        Me.ItemsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegisterToolStripMenuItem, Me.SearchToolStripMenuItem1, Me.UpdateToolStripMenuItem})
        Me.ItemsToolStripMenuItem.Name = "ItemsToolStripMenuItem"
        Me.ItemsToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.ItemsToolStripMenuItem.Text = "Items"
        '
        'RegisterToolStripMenuItem
        '
        Me.RegisterToolStripMenuItem.Name = "RegisterToolStripMenuItem"
        Me.RegisterToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.RegisterToolStripMenuItem.Text = "Register"
        '
        'SearchToolStripMenuItem1
        '
        Me.SearchToolStripMenuItem1.Name = "SearchToolStripMenuItem1"
        Me.SearchToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.SearchToolStripMenuItem1.Text = "Search"
        '
        'UpdateToolStripMenuItem
        '
        Me.UpdateToolStripMenuItem.Name = "UpdateToolStripMenuItem"
        Me.UpdateToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.UpdateToolStripMenuItem.Text = "Update"
        '
        'PurchaseToolStripMenuItem
        '
        Me.PurchaseToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MakeTransactionToolStripMenuItem})
        Me.PurchaseToolStripMenuItem.Name = "PurchaseToolStripMenuItem"
        Me.PurchaseToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PurchaseToolStripMenuItem.Text = "Purchase"
        '
        'SellToolStripMenuItem
        '
        Me.SellToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MakeTransactionToolStripMenuItem1})
        Me.SellToolStripMenuItem.Name = "SellToolStripMenuItem"
        Me.SellToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SellToolStripMenuItem.Text = "Sell"
        '
        'MakeTransactionToolStripMenuItem
        '
        Me.MakeTransactionToolStripMenuItem.Name = "MakeTransactionToolStripMenuItem"
        Me.MakeTransactionToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.MakeTransactionToolStripMenuItem.Text = "Make Transaction"
        '
        'MakeTransactionToolStripMenuItem1
        '
        Me.MakeTransactionToolStripMenuItem1.Name = "MakeTransactionToolStripMenuItem1"
        Me.MakeTransactionToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.MakeTransactionToolStripMenuItem1.Text = "Make Transaction"
        '
        'MDI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(909, 390)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Name = "MDI"
        Me.Text = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransactionTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchTransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogOutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegisterSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegisterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SellToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MakeTransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MakeTransactionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem

End Class
